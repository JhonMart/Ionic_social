import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';

@Injectable()
export class FirebaseDbProvider {
  path = '/users/';
  constructor(private db: AngularFireDatabase) {}

  public getUsers(){
    return this.db.object(this.path).snapshotChanges();
  }

  public saveUser(data: Object, id: string = null){
    return new Promise((resolve, reject)=>{
      if(id){
        return this.db.list(this.path).update(id, data)
        .then(()=> resolve())
        .catch((e) => reject(e));
      } else{
        return this.db.list(this.path).push(data)
        .then(()=> resolve());
      }
    })
  }

  public delteUser(key){
    return this.db.list(this.path).remove(key);
  }

}
