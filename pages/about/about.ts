import { Component, OnInit } from '@angular/core';
import { NavController, AlertController, ActionSheetController } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';
import { FirebaseDbProvider } from '../../providers/firebase-db/firebase-db';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage implements OnInit {
	users: any[];
  constructor(public navCtrl: NavController, 
              public service : ServiceProvider,
              public alertCtrl: AlertController,
              public actionSheetCtrl: ActionSheetController,
              public firebaseDb: FirebaseDbProvider) {
  }

  ngOnInit(){
    this.getDados();
  }
    
  getDados(){
   	this.getAllUsers();
   }

   public getAllUsers(){
     
    this.firebaseDb.getUsers().subscribe(action => {
      var valor = action.payload.val();
      this.users = []; // Resetar valor
      if(valor!=null){
        Object.keys(valor).map(e=>{
          valor[e].id = e;
          this.users.push(valor[e]);
        });
      }
    })
  }

   editarPerfil(req) {

    let prompt = this.alertCtrl.create({
      title: 'Perfil do usuário',
      message: "Altere Informações dos usuários",
      inputs: [
        {
          name: 'nome',
          placeholder: 'Nome',
          value:req.nome
        },
        {
          name: 'email',
          placeholder: 'Email',
          value:req.email
        },
        {
          name: 'profissao',
          placeholder: 'Profissão',
          value:req.profissao
        },
        {
          name: 'telefone',
          placeholder: 'Telefone',
          value:req.telefone
        }
      ],
     
    });
    prompt.present();
  }
  
}
