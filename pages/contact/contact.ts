import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { NavController, AlertController, ToastController, Platform } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';
import { FirebaseDbProvider } from '../../providers/firebase-db/firebase-db';
import { AboutPage } from '../about/about';
import { Geolocation } from '@ionic-native/geolocation';
import { LoadingController } from 'ionic-angular';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})

export class ContactPage implements OnInit {
	users : any[];
	cadastro : any = {};
	lat;
	lng;

	constructor(public navCtrl: NavController,
							public service : ServiceProvider,
							public formBuilder: FormBuilder,
							public alertCtrl: AlertController,
							public toastCtrl: ToastController,
							public firebaseDb: FirebaseDbProvider,
							public geolocation: Geolocation, 
							public platform:Platform,
							public load : LoadingController) {
		this.getDados();
		this.cadastro = this.formBuilder.group({
			nome:['', Validators.required],
			email:['', Validators.required],
      profissao:['', Validators.required],
			cod:['', Validators.required],
			telefone:['', Validators.required],
			lat:['', Validators.required],
			lng:['', Validators.required]
		});

  }
	ngOnInit(){
		this.getDados();
	}

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      this.getDados();	
      console.log('Async operation has ended');
      refresher.complete();
    }, 1000);
	}
	
  getDados(){
   	this.service.getData()
		.subscribe(
			data => this.users = data,
			err => console.log(err)
		);
	}

	localizar(){
		let loadLocal = this.load.create({content:'Aguarde ...'});
		let toast = this.toastCtrl.create({
			message: 'GPS desativado. Verifique!',
			duration: 2000
		});

		this.platform.ready().then(() => {
			loadLocal.present();

			setTimeout(function(){loadLocal.dismiss()},5000);
      this.geolocation.getCurrentPosition().then(result => {
        this.lat = result.coords.latitude;
        this.lng = result.coords.longitude;
				loadLocal.dismiss();
      }).catch(function(e) {
				loadLocal.dismiss();
				toast.present();
      });
		});
		
	}

	postDados(){
		this.firebaseDb.saveUser(this.cadastro.value).then(() => {
      let toast = this.toastCtrl.create({
				message: 'Usuário cadastrado com sucesso',
				duration: 2000
			});
			toast.present();
			this.navCtrl.setRoot(AboutPage);
    })
    .catch(erro =>
          {
            this.alertCtrl.create(
              {
                title : 'Erro na API',
                buttons : [{text : 'Ok'}],
                subTitle : 'Tente novamente mais tarde!'
              }
            ).present();
          }
    );
	}
}