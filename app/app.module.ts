import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';


import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ServiceProvider } from '../providers/service/service';
import { HttpModule } from '@angular/http';
import { FirebaseDbProvider } from '../providers/firebase-db/firebase-db';
import { Geolocation } from '@ionic-native/geolocation';
import { DetalheRotaPage } from '../pages/detalhes-rota/detalhes-rota';

export const firebaseConfig = {
  apiKey: "AIzaSyDwR4X5Tu3I8Gd0lU8DXo2mhzYnJFXce8Q",
  authDomain: "faculdade-social.firebaseapp.com",
  databaseURL: "https://faculdade-social.firebaseio.com",
  projectId: "faculdade-social",
  storageBucket: "faculdade-social.appspot.com",
  messagingSenderId: "150481630882"
};

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    DetalheRotaPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    DetalheRotaPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AngularFireDatabase,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ServiceProvider,
    FirebaseDbProvider,
    Geolocation
  ]
})
export class AppModule {}
